<?php

namespace App\Controller;

use App\Entity\User;
use App\Services\GiftService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index(GiftService $gifts)
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();

        $this->addFlash(
            'notice', 'Notice Flash message'
        );

        $session = new Session();

//        $session->set('name', 'Drak');
//        $session->get('name');

        $session->getFlashBag()->add('key', 'value');

        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'users' => $users,
            'gifts' => $gifts->gifts,
            'session' => $session
        ]);
    }
}
